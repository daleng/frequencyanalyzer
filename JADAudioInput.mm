//
//  JADAudioInput.mm
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import "JADAudioInput.h"

#include "AudioInput.h"

// C++ callback wrapper
class AudioInputConsumerImpl : public AudioInputConsumer {
public:
	AudioInputConsumerImpl(JADAudioInput* inWrappedObjcObject) : wrappedObjcObject(inWrappedObjcObject) {
	
	}
	virtual void AudioReady(float* data, int numberOfSamples) {
        @autoreleasepool {
            [[wrappedObjcObject targetAudioConsumer]  audioReady: data numberOfSamples: numberOfSamples];            
        }
	}
private:
	JADAudioInput* wrappedObjcObject;
};

@implementation JADAudioInput

- (id) initWithConsumer:(id<JADAudioConsumer>) audioConsumer {
	self = [super init];
	if (self != nil) {
		targetAudioConsumer = audioConsumer;
		cppWrapper = new AudioInputConsumerImpl(self);
		cppAudioInput = new AudioInput(*(AudioInputConsumerImpl*)cppWrapper);
	}
	return self;	
}
		 
-(id<JADAudioConsumer>) targetAudioConsumer {
	return targetAudioConsumer;
}

- (void) dealloc {
	delete ((AudioInputConsumerImpl*)cppWrapper);
	delete ((AudioInput*)cppAudioInput);
}


@end
