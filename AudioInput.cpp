/*
 *  AudioInput.cpp
 *  FrequencyAnalyzer
 *
 *  Created by Johannes Alming Daleng on 4/12/10.
 *  Copyright 2010. All rights reserved.
 *
 */

#include "AudioInput.h"

#include <stdexcept>

AudioInput::AudioInput(AudioInputConsumer& inAudioInputConsumer) : audioInputConsumer(inAudioInputConsumer) {
	buffers = NULL;
	SetupAudioUnits();
}

AudioInput::~AudioInput() {
	AudioOutputUnitStop(audioUnit);
    TeardownAudioUnits();
    DeallocateBuffers();
}

OSStatus AudioInput::AudioInputCallback(void* inRefCon, AudioUnitRenderActionFlags* ioActionFlags, const AudioTimeStamp* inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList* ioData) {
	((AudioInput*)inRefCon)->InputCallback(ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, ioData);
	return noErr;
}

void AudioInput::InputCallback(AudioUnitRenderActionFlags* ioActionFlags, const AudioTimeStamp* inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList* ioData) {
	if (buffers == NULL) {
		AllocateBuffers(channelCount, inNumberFrames);
	}
	if (AudioUnitRender(audioUnit, ioActionFlags, inTimeStamp, inBusNumber, inNumberFrames, buffers) != noErr) {
		printf("AudioInput::InputCallback failed to fill audio buffer");
		return;
	}
	
	float* sampleData = (float*)buffers->mBuffers[0].mData;
	audioInputConsumer.AudioReady(sampleData, inNumberFrames);
}

void AudioInput::SetupAudioUnits() {
	// Find default intput device and set up AUHAL uinit
	AudioComponent comp;
    AudioComponentDescription desc;
	
    desc.componentType = kAudioUnitType_Output;
	desc.componentSubType = kAudioUnitSubType_HALOutput;
	
    desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    desc.componentFlags = 0;
    desc.componentFlagsMask = 0;
	
    comp = AudioComponentFindNext(NULL, &desc);
    if (comp == NULL) throw std::runtime_error("AudioInput::SetupAudioUnits() failed to find audio input device");
	
    AudioComponentInstanceNew(comp, &audioUnit);
	
	
	UInt32 enableIO;
	enableIO = 1;
	AudioUnitSetProperty(audioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Input, 1, &enableIO, sizeof(enableIO));
	
	enableIO = 0;
	AudioUnitSetProperty(audioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, 0, &enableIO, sizeof(enableIO));
	
	UInt32 size = sizeof(AudioDeviceID);
	AudioDeviceID inputDevice = 0;
    
    UInt32 inputDeviceIdSize = sizeof(AudioDeviceID);
    AudioObjectPropertyAddress defaultInputDeviceAddress = {
        kAudioHardwarePropertyDefaultInputDevice,
        kAudioObjectPropertyScopeGlobal,
        kAudioObjectPropertyElementMaster
    };
    if ( AudioObjectGetPropertyData(kAudioObjectSystemObject,
                                    &defaultInputDeviceAddress,
                                    0,
                                    NULL,
                                    &inputDeviceIdSize,
                                    &inputDevice) != noErr ) {
		throw std::runtime_error("AudioInput::SetupAudioUnits() Failed to get default input device");
    }
    
	if ( AudioUnitSetProperty(audioUnit, kAudioOutputUnitProperty_CurrentDevice, kAudioUnitScope_Global, 0, &inputDevice, sizeof(inputDevice)) != noErr)
		throw std::runtime_error("AudioInput::SetupAudioUnits() Failed to set default input device");
	
	AudioStreamBasicDescription currentFormat;
    AudioStreamBasicDescription desiredFormat;
	size = sizeof(AudioStreamBasicDescription);
	
	//Get the input device format
    AudioUnitGetProperty (audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, 1, &currentFormat, &size);
	
    // set the desired format 
	channelCount = 1; // Mono
    desiredFormat.mSampleRate =  currentFormat.mSampleRate;
	desiredFormat.mChannelsPerFrame = channelCount;
	desiredFormat.mFormatID = kAudioFormatLinearPCM;
	desiredFormat.mFormatFlags = kAudioFormatFlagIsFloat | kAudioFormatFlagIsPacked | kAudioFormatFlagIsNonInterleaved;
	
	desiredFormat.mBitsPerChannel = sizeof(Float32) * 8;
	desiredFormat.mBytesPerFrame = desiredFormat.mBitsPerChannel / 8;
	desiredFormat.mFramesPerPacket = 1;
	desiredFormat.mBytesPerPacket = desiredFormat.mBytesPerFrame;
	
	
	//set format to output scope
    AudioUnitSetProperty(audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Output, 1, &desiredFormat, sizeof(AudioStreamBasicDescription));
	
	
	// Wire up callback
	AURenderCallbackStruct input;
    input.inputProc = AudioInput::AudioInputCallback;
    input.inputProcRefCon = this;
	
    AudioUnitSetProperty(audioUnit, kAudioOutputUnitProperty_SetInputCallback, kAudioUnitScope_Global, 0, &input, sizeof(input));
	
	AudioUnitInitialize(audioUnit);
	AudioOutputUnitStart(audioUnit);
}

void AudioInput::TeardownAudioUnits() {
    AudioComponentInstanceDispose(audioUnit);
    audioUnit = 0;
}

void AudioInput::AllocateBuffers(int channelCount, int samplesPerChannel) {
	buffers = (AudioBufferList*)malloc(sizeof(AudioBufferList) + channelCount * sizeof(AudioBuffer));
	
	buffers->mNumberBuffers = channelCount;
	for(int i = 0; i < channelCount; ++i) {
		buffers->mBuffers[i].mNumberChannels = 1;
		buffers->mBuffers[i].mDataByteSize = samplesPerChannel*sizeof(float);
		buffers->mBuffers[i].mData = malloc(samplesPerChannel*sizeof(float));
	}
}

void AudioInput::DeallocateBuffers() {
	buffers->mNumberBuffers = channelCount;
	for(int i = 0; i < buffers->mNumberBuffers; ++i) {
		free(buffers->mBuffers[i].mData);
	}
	free(buffers);
}





