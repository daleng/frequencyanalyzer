//
//  JADAudioInput.h
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

/**
 * protocol JADAudioConsumer
 *
 * Objects conforming to this protocol can recie audio data using the JADAudioInput class.
 *
 */
@protocol JADAudioConsumer
@required
- (void) audioReady:(float*) data numberOfSamples:(int) sampleCount;
@end

/**
 * class JADAudioInput
 *
 * Simple class providing mono audio input from the default input device using CoreAudio/AUHAL.
 * The class wraps a C++ class (AudioInput) doing the actual CoreAudio stuff.
 *
 */
@interface JADAudioInput : NSObject {
@private
	id<JADAudioConsumer> targetAudioConsumer;
	void* cppWrapper;
	void* cppAudioInput;
}

-(id)initWithConsumer:(id<JADAudioConsumer>) audioConsumer;
-(id<JADAudioConsumer>) targetAudioConsumer;

@end
