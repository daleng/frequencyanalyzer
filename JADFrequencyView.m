//
//  JADFrequencyView.m
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import "JADFrequencyView.h"

const size_t fftLenght=1024;
const size_t scopeLenght=128;// Note: scopeLenght must be less than or equal to fftLenght

#include <OpenGL/glu.h>
#include <mm_malloc.h>

@implementation JADFrequencyView  {
	JADAudioInput* audioInput;
	
	NSTimer* timer;
	
	// Buffers
	size_t sampleCount;
	float* timeDomainBuffer;
	float* frequencyDomainBufferReal;
	float* frequencyDomainBufferImag;
	float* fftTempBufferReal;
	float* fftTempBufferImag;
	
	// Viewport info
	GLfloat width;
	GLfloat width_2;
	GLfloat height;
	GLfloat height_2;
	
	// Animation params
	GLfloat rotation;
	GLfloat fftAnimProgress;
	GLfloat scopeAnimProgress;
	GLfloat fftLift;
	GLfloat scopeLift;
	int mode;
	
	// FFT plotting ranges
	GLfloat startFrequency;
	GLfloat endFrequency;
	GLfloat minDb;
	GLfloat maxDb;
	
	
	GLuint fftTexture;
	GLuint scopeTexture;
	
	FFTSetup fftSetup;
	
	pthread_mutex_t audioDataMutex;
}

+ (NSOpenGLPixelFormat*)defaultPixelFormat {
    NSOpenGLPixelFormatAttribute attributes [] = {
		NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, 32,
		NSOpenGLPFASampleBuffers, 1,
		NSOpenGLPFASamples, 4,
        NSOpenGLPFASampleAlpha,
        NSOpenGLPFASupersample,
		(NSOpenGLPixelFormatAttribute)nil
	};
    return [[NSOpenGLPixelFormat alloc] initWithAttributes:attributes];
}
-(id) initWithFrame: (NSRect) frame {
    NSOpenGLPixelFormat *pixelFormat = [JADFrequencyView defaultPixelFormat];
    self = [super initWithFrame: frame pixelFormat: pixelFormat];
	audioInput = nil;
	timeDomainBuffer = NULL;
	frequencyDomainBufferReal = NULL;
	frequencyDomainBufferImag = NULL;
	fftTempBufferReal = NULL;
	fftTempBufferImag = NULL;

	return self;
}

-(void)awakeFromNib {
	[self allocateBuffers];
	[self initFFT];
	
	startFrequency = 20;
	endFrequency = 20000;
	minDb = -40;
	maxDb = 30;
	fftTexture = 0;
	scopeTexture = 0;
	fftAnimProgress = 0;
	scopeAnimProgress = 0;
	
	timer = [NSTimer timerWithTimeInterval: 1.0 / 60. target:self selector:@selector(updateAnimations:) userInfo:nil repeats: YES];
	[[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
	audioInput = [[JADAudioInput alloc] initWithConsumer: self];
	
	
}


- (void)windowWillClose:(NSNotification *)notification {
	
}

-(void) updateAnimations:(NSTimer*) theTimer {
	GLfloat rate = [theTimer timeInterval]*2.f;
	if ( mode == 0 ) {
		fftAnimProgress -= rate;
		scopeAnimProgress -= rate;
	} else if ( mode == 1 ) {
		fftAnimProgress += rate;
		scopeAnimProgress -= rate;
	} else {
		fftAnimProgress -= rate;
		scopeAnimProgress += rate;
	}
	scopeAnimProgress = MAX(0,scopeAnimProgress);
	scopeAnimProgress = MIN(1,scopeAnimProgress);
	fftAnimProgress = MAX(0,fftAnimProgress);
	fftAnimProgress = MIN(1,fftAnimProgress);
	
	scopeLift = -0.5f*cosf(scopeAnimProgress*M_PI)+0.5f;
	fftLift = -0.5f*cosf(fftAnimProgress*M_PI)+0.5f;
	
	[self setNeedsDisplay:YES];
}


- (void) allocateBuffers {
	timeDomainBuffer = (float*) malloc(fftLenght*sizeof(float));
	frequencyDomainBufferReal = (float*) malloc(fftLenght*sizeof(float));
	frequencyDomainBufferImag = (float*) malloc(fftLenght*sizeof(float));
    memset(timeDomainBuffer, 0, fftLenght*sizeof(float));
    memset(frequencyDomainBufferReal, 0, fftLenght*sizeof(float));
    memset(frequencyDomainBufferImag, 0, fftLenght*sizeof(float));
}
- (void) deallocateBuffers {
	free(timeDomainBuffer);
	free(frequencyDomainBufferReal);
	free(frequencyDomainBufferImag);
}

- (void) dealloc {
	[self destroyFFT];
	[self deallocateBuffers];
}

- (void) audioReady:(float*) data numberOfSamples:(int) inSampleCount {
	int samplesRemaining = fftLenght-sampleCount;
	int samplesToCopy = MIN(samplesRemaining, inSampleCount);
	memcpy(timeDomainBuffer + sampleCount, data, samplesToCopy*sizeof(float));
	sampleCount+=samplesToCopy;
	if ( sampleCount >= fftLenght ) {
		[self performFFT];
		sampleCount = 0;
		if (samplesToCopy < inSampleCount) {
			// Copy remaining data
			samplesToCopy = MIN(fftLenght-sampleCount, inSampleCount-samplesToCopy);
			memcpy(timeDomainBuffer + sampleCount, data, samplesToCopy*sizeof(float));
			sampleCount+=samplesToCopy;
		}
	}
}

-(void)prepareOpenGL {
    glEnable(GL_MULTISAMPLE);
}

- (void)drawRect:(NSRect)dirtyRect {
	[[self openGLContext] makeCurrentContext];
    
    // Clear.
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	    
	// Enable blending and smoothing
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	
    glHint (GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
	// Set up perspective projection
	GLfloat fov = 80.f; // 80 degrees vertical field of view
	GLfloat viewingDistance = height_2 / tanf(fov * M_PI_2 * (1.f/180.f));
	gluPerspective(fov, width/height, viewingDistance / 10.f, viewingDistance * 10.f);
	
	glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
	gluLookAt(0,0,viewingDistance,0,0,0,0,1,0);

	glEnable(GL_DEPTH_TEST);

	// Draw scope
	glPushMatrix();
	glTranslatef(-width_2*(1.f-scopeLift), 0, (scopeLift*viewingDistance)-viewingDistance);
    glRotatef(10*(1-scopeLift), 0, 1, 0);
	[self drawScope:1.f];
	glPopMatrix();
	// Scope mirror
	if (scopeLift < 1) {
		glPushMatrix();
		glTranslatef(-width_2*(1.f-scopeLift), -height, (scopeLift*viewingDistance)-viewingDistance);
		glScalef(1, -1, 1);
		glRotatef(10*(1-scopeLift), 0, 1, 0);
		[self drawScope:0.4f];
		glPopMatrix();
	}
	

	// Draw FFT
	glPushMatrix();
	glTranslatef(width_2*(1.f-fftLift), 0, (fftLift*viewingDistance)-viewingDistance);
	glRotatef(-10*(1-fftLift), 0, 1, 0);
	[self drawFFT:1.f];
	glPopMatrix();
	// FFT mirror
	if (fftLift < 1) {
		glPushMatrix();
		glTranslatef(width_2*(1.f-fftLift), -height, (fftLift*viewingDistance)-viewingDistance);
		glScalef(1, -1, 1);
		glRotatef(-10*(1-fftLift), 0, 1, 0);
		[self drawFFT:0.4f];
		glPopMatrix();
	}
    
	//glFlush();
	
	[[self openGLContext] flushBuffer];
}

- (void) drawScope:(float) alpha {
	glPushMatrix();
	GLfloat xScale = (0.7f + 0.3f*scopeLift)* width / scopeLenght;
	GLfloat yScale = height_2;
	glScalef(xScale, yScale, 1);
	glTranslatef(-(scopeLenght/2.f), 0, 0);
	
	// Background and frame
	glBegin(GL_QUADS);
	glColor4f(0.1f, 0.1f, 0.1f, alpha);
	glVertex3f(0, 1, -0.1f);
	glVertex3f(scopeLenght, 1, -0.1f);
	glColor4f(0.14f, 0.14f, 0.14f, alpha);
	glVertex3f(scopeLenght, -1, -0.1f);
	glVertex3f(0, -1, -0.1f);
	glEnd();
	
	glLineWidth(1.f);
	glBegin(GL_LINE_LOOP);
	glColor4f(1,1,1,alpha);
	glVertex2f(0, 1);
	glVertex2f(scopeLenght, 1);
	glVertex2f(scopeLenght, -1);
	glVertex2f(0, -1);
	glEnd();

    // Top clip equation
    // Ax + By + Cz + D = 0
    // Normal: 0,-1,0 (A,B,C), anchorpoint 0,1,0
    // Solve: 0 * 0 + (-1 * 1) + 0 * 0 + D = 0  => D = 1.
    GLdouble topClipEquation[4] = { 0.0, -1.0, 0.0, 1.0 };
    glClipPlane(GL_CLIP_PLANE0, topClipEquation);
    glEnable(GL_CLIP_PLANE0);

    // Normal: 0,1,0, anchorpoint 0,-1,0
    // Solve: 0 * 0 + (1 * -1) + 0 * 0 + D = 0  => D = 1.
    GLdouble bottomClipEquation[4] = { 0.0, 1.0, 0.0, 1.0 };
    glClipPlane(GL_CLIP_PLANE1, bottomClipEquation);
    glEnable(GL_CLIP_PLANE1);
    
	GLfloat plotGain = 10.f;
	glScalef(1, plotGain, 1);

    
	glColor4f(0.2f, 1.f, 0.2f, alpha);
	glLineWidth(1.0);
	glBegin(GL_LINE_STRIP);
    int triggerOffset = [self calcScopeTriggerOffset];
	for (int n=0; n<scopeLenght; ++n) {
		glVertex2f(n, timeDomainBuffer[n+triggerOffset]);
	}
	
	glEnd();
	
    glDisable(GL_CLIP_PLANE0);
    glDisable(GL_CLIP_PLANE1);
    glPopMatrix();
}

- (int) calcScopeTriggerOffset {
    // Simulate positive edge scope trigger, triggerlevel = 0
    int maxOffset = fftLenght - scopeLenght;
    for (int i=1; i < maxOffset; ++i) {
        if ( timeDomainBuffer[i-1] < 0 && timeDomainBuffer[i] >= 0 ) return i;
    }
    return 0;
}

- (void)mouseDown:(NSEvent *)theEvent {
	mode++;
	if (mode>2) mode = 0;
}
 
- (void) drawFFT:(float) alpha {
	glPushMatrix();
	const GLfloat sampleFrequency = 44100.f; // TODO: fix hardcoded sample frequency
	
	// Scale x any coordinates to match dB and frequency values
	GLfloat logStartFreq = log10f(startFrequency);
	GLfloat logEndFreq = log10f(endFrequency);
	GLfloat xScale = (0.7f + 0.3f*fftLift) * width / (logEndFreq-logStartFreq);
	GLfloat yScale = height / (maxDb-minDb);
	
	glTranslatef(-((0.7f + 0.3f*fftLift) * width_2), -height_2, 0);
	glScalef(xScale, yScale, 1);
	glTranslatef(-logStartFreq, -minDb, 0);
	
	[self drawFFTbox:alpha];
	
	glColor4f(0, 1, 0, alpha);
	glLineWidth(1.0);
    
	glBegin(GL_LINE_STRIP);
	
	for (int n=1; n<fftLenght/2; ++n) {
		GLfloat frequency = (n*sampleFrequency) / fftLenght;
		GLfloat x = log10f(frequency);
		GLfloat absValue = sqrtf(frequencyDomainBufferReal[n]*frequencyDomainBufferReal[n]+frequencyDomainBufferImag[n]*frequencyDomainBufferImag[n]);
		GLfloat dbValue = 20*log10f(absValue);
		if (isfinite(dbValue) && !isnan(dbValue) && isnormal(dbValue) && dbValue < maxDb && dbValue > minDb)
			glVertex3f(x, dbValue, 0.1f);
	}
	
	glEnd();
	glPopMatrix();
}

- (void) drawFFTbox:(float) alpha {
	GLfloat dbLineSpacing = 15;
	GLfloat logStartFreq = log10f(startFrequency);
	GLfloat logEndFreq = log10f(endFrequency);
	
	// Background and frame
	glBegin(GL_QUADS);
	glColor4f(0.1f, 0.1f, 0.1f, alpha);
	glVertex3f(logStartFreq, maxDb, -1.f);
	glVertex3f(logEndFreq, maxDb, -1.f);
	glColor4f(0.15f, 0.15f, 0.15f, alpha);
	glVertex3f(logEndFreq, minDb, -1.f);
	glVertex3f(logStartFreq, minDb, -1.f);
	glEnd();
	
	glLineWidth(1.f);
	glBegin(GL_LINE_LOOP);
	glColor4f(1,1,1,alpha);
	glVertex2f(logStartFreq, maxDb);
	glVertex2f(logEndFreq, maxDb);
	glVertex2f(logEndFreq, minDb);
	glVertex2f(logStartFreq, minDb);
	glEnd();
	
	// dB lines
	glLineWidth(1.f);
	glBegin(GL_LINES);
	for ( int db = minDb; db<=maxDb; db+=dbLineSpacing) {
		if (db==0)
			glColor4f(1.f, 1.f, 1.f, alpha);
		else 
			glColor4f(1.f, 1.f, 1.f, 0.3f*alpha);

		glVertex2f(logStartFreq, (GLfloat)db);
		glVertex2f(logEndFreq, (GLfloat)db);
	}
	glEnd();
	
	// Logarithmic frequency lines
	int frequencyIncrement = 10;
	glColor4f(1.f, 1.f, 1.f, 0.3f*alpha);
	glLineWidth(1.f);
	glBegin(GL_LINES);
	for (int frequency = startFrequency; frequency <= endFrequency; frequency += frequencyIncrement) {
		GLfloat logFreq = log10f((GLfloat)frequency);
		glVertex2f(logFreq, maxDb);
		glVertex2f(logFreq, minDb);
		if ( frequency >= frequencyIncrement*10 ) frequencyIncrement *=10;
	}
	glEnd();
}

- (void) triggerRedraw {
	[self setNeedsDisplay:YES];
}

- (void) reshape {
    [[self openGLContext] makeCurrentContext];
    
    // Get the view size in screen coordinates.
    NSSize size = [self convertSize:[self bounds].size toView:nil];
    glViewport(0, 0, size.width, size.height);
	width = size.width;
	height = size.height;
	width_2 = width / 2.f;
	height_2 = height / 2.f;
}


- (void) initFFT {
	fftSetup = vDSP_create_fftsetup((int)log2(fftLenght),kFFTRadix2);
	// 32 byte aligned temp buffers (Better performance according to Accellerate framework doc)
	fftTempBufferReal = (float*)_mm_malloc(fftLenght*sizeof(float), 32);
	fftTempBufferImag = (float*)_mm_malloc(fftLenght*sizeof(float), 32); 

}

- (void) destroyFFT {
	vDSP_destroy_fftsetup(fftSetup);
	_mm_free(fftTempBufferReal);
	_mm_free(fftTempBufferImag);
}

- (void) performFFT {
	
	DSPSplitComplex timeData;
	timeData.realp = timeDomainBuffer;
	timeData.imagp = timeDomainBuffer;
	DSPSplitComplex freqData;
	freqData.realp = frequencyDomainBufferReal;
	freqData.imagp = frequencyDomainBufferImag;
	DSPSplitComplex tempBuffer;
	tempBuffer.realp = fftTempBufferReal;
	tempBuffer.imagp = fftTempBufferImag;	
	
	vDSP_fft_zopt(fftSetup, &timeData, 1, &freqData, 1, &tempBuffer,(int)log2(fftLenght), kFFTDirection_Forward);
}

@end
