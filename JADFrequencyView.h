//
//  JADFrequencyView.h
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import "JADAudioInput.h"

#import <Cocoa/Cocoa.h>
#import <Accelerate/Accelerate.h>
#import <OpenGL/OpenGL.h>


/**
 * class JADFrequencyView
 *
 * View class displaying a scope type display of audio data, or an fourier
 * transformed view of the same audio data, or both at the same time.
 *
 * Click in the view to cycle through the display modes.
 *
 */

@interface JADFrequencyView : NSOpenGLView <JADAudioConsumer>{
}


@end
