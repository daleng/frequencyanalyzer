//
//  FrequencyAnalyzerAppDelegate.h
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface FrequencyAnalyzerAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *__weak window;
}

@property (weak) IBOutlet NSWindow *window;

@end
