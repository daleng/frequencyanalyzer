/*
 *  AudioInput.h
 *  FrequencyAnalyzer
 *
 *  Created by Johannes Alming Daleng on 4/12/10.
 *  Copyright 2010. All rights reserved.
 *
 */

#include <AudioUnit/AudioUnit.h>
#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>

class AudioInputConsumer;

/**
 * Simple class providing mono audio input from the default input device using CoreAudio/AUHAL.
 *
 * The audio stream starts immediatly after initializaiton, and is stopped at deallocation.
 */
class AudioInput {
public:
	
	AudioInput(AudioInputConsumer& audioInputConsumer);
	virtual ~AudioInput();
	
private:
	void SetupAudioUnits();
    void TeardownAudioUnits();
	void AllocateBuffers(int channelCount, int samplesPerChannel);
	void DeallocateBuffers();
	
	AudioInputConsumer& audioInputConsumer;
	AudioUnit audioUnit;
	AudioBufferList* buffers;
	size_t channelCount;
	static OSStatus AudioInputCallback(void* inRefCon, AudioUnitRenderActionFlags* ioActionFlags, const AudioTimeStamp* inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList* ioData);	
	void InputCallback(AudioUnitRenderActionFlags* ioActionFlags, const AudioTimeStamp* inTimeStamp, UInt32 inBusNumber, UInt32 inNumberFrames, AudioBufferList* ioData);
};

class AudioInputConsumer {
public:
	virtual void AudioReady(float* data, int numberOfSamples)=0;
	virtual ~AudioInputConsumer() {}
};