//
//  FrequencyAnalyzerAppDelegate.m
//  FrequencyAnalyzer
//
//  Created by Johannes Alming Daleng on 4/12/10.
//  Copyright 2010. All rights reserved.
//

#import "FrequencyAnalyzerAppDelegate.h"

@implementation FrequencyAnalyzerAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

}

- (BOOL) applicationShouldTerminateAfterLastWindowClosed: (NSApplication *) theApplication {
	return YES;
}

@end
